import { Component, EventEmitter, Output } from '@angular/core';
import { Country } from '../../interfaces/countries';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'app-by-capital-page',
  templateUrl: './by-capital-page.component.html',
  styles: [
  ]
})
export class ByCapitalPageComponent {
  public countries: Country[] = []
  constructor( private countriesService: CountriesService ){

  }

  searchByCapital(value: string):void {
    this.countriesService.searchCapital(value)
      .subscribe(countries => {
         this.countries = countries
      })
        console.log("Value:",value);
  }
}
