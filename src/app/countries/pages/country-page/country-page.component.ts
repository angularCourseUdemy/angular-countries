import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { Country } from '../../interfaces/countries';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'app-country-page',
  templateUrl: './country-page.component.html',
  styles: [
  ]
})
export class CountryPageComponent implements OnInit {
  public country?: Country;
  constructor(
    private activatedRoute : ActivatedRoute,
    private router : Router,
    private countriesService: CountriesService
  ){

  }
  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        switchMap( ({ id }) => this.countriesService.searchCountryCode(id) ),
      )
      .subscribe(resp => {
        if(!resp) {
          return this.router.navigateByUrl('home')
        }
        return this.country = resp
        return;
        console.log(resp);
      });
    }

}
