import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'shared-search-box',
  templateUrl: './search-box.component.html',
  styles: [
  ]
})
export class SearchBoxComponent {
 @Input()
 public placeholder: string = '';

 @Output()
    public onValue: EventEmitter<string> = new EventEmitter();

  emitValue(event: KeyboardEvent):void {
    if (event.key !== 'Enter') return
    const input = document.getElementById('textInput') as HTMLInputElement
    this.onValue.emit(input.value)
  }
}
